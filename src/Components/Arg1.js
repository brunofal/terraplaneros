import React from 'react';


const Arg1 = () => {
    return (
        <div className="container mt-5 mb-5">
            <div className="row align-middle">

                 <div className="col">

                    <h2>Si la tierra es redonda...</h2>
                    <p>
                    ...su curvatura debería ser visible desde cierta altura ¡Sin embargo cuando vemos el horizonte no se aprecia ninguna!
                    Incluso más cuando se viaja en avión -a unos 10km de altura- dicha curvatura debería saltar a la vista.
                    </p>
                   
                    <p>
                    En el siguiente cuadro puede observar los complejos calculos trogonométricos que se aplican para un modelo esférico
                    </p>

                                
                </div>

                <div className="col d-flex align-items-center">
                <div className="d-flex justify-content-center align-middle">

        <img src="https://steemitimages.com/p/2Qhhdda6Qnbf3tYLbS1VUc13o3pdfoPAPAYoKEvj1m2oBuWmie49stB2ieCmSy3fLFnRFasNNMLu8JPCdnnJ?format=match&mode=fit&width=1280" className="rounded img-fluid" alt="Imagencita"/>

        </div>

                </div>
            </div>
            <h4 className="mt-5">
                En el siguiente video se puede ver como - con suficiente aumento - se puede ver un objeto que debería ser invisible
                debido a la curvatura.
            </h4>
            <div className="d-flex justify-content-center mt-5">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/jC8ek0ICAoI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
      );
}
 
export default Arg1;