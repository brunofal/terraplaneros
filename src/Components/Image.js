import React from 'react';

const Image = () => {
    return ( 
        <div className="d-flex justify-content-center align-middle">

        <img src="https://grupomarmor.com.mx/wp-content/uploads/2020/02/Tierra-plana-954x420.png" className="rounded img-fluid" alt="Imagencita"/>

        </div>
     );
}
 
export default Image;
