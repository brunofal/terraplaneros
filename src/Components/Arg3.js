import React from 'react';

const Arg3 = () => {
    return (
        <div className="container mt-5 mb-5">
            <div className="row align-middle">

                <div className="col">

                    <h2>¿Cómo se explica la gravedad?</h2>
                    <p>
                    Mucha gente nos pregunta: Si la tierra realmente es plana ¿Cómo explican la gravedad? Ja! otro invento mentiroso
                    de los masones. No existe tal cosa como la gravedad.
                    </p>

                    <h5>
                        ¡Pero cuando tirás algo para arriba, este cae! Esto se explica porque nuestro planeta está moviendose constantemente hacia arriba, viajando
                        por el espacio y esto es lo que provoca que nos quedemos pegados al suelo. Fácil, ¿no? Como cuando subis en un ascensor.
                </h5>


                </div>

                <div className="col d-flex align-items-center">
                    <div className="d-flex justify-content-center align-middle">

                        <img src="https://cdn.mos.cms.futurecdn.net/kotQRjJDbBGRMBEeF8vEt3-970-80.jpg" className="rounded img-fluid" alt="Imagencita" />

                    </div>

                </div>
            </div>
       
        </div>
    );
}

export default Arg3;