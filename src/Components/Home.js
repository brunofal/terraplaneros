import React from 'react';
import Image from './Image'
const Home = () => {
    return (

        <div className="container mt-5 mb-5">
            <div className="row align-middle">

                 <div className="col">

                    <h2>Te han estado mintiendo</h2>
                    <p>
                        A todos en la escuela nos enseñaron que la tierra es redonda, pero eso no lo vuelve necesariamente cierto. 
                        ¡Atrévete a desafiar los conocimientos pre-establecidos y buscar la verdad!
                    </p>
                    <p>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eligendi dolorum, quaerat incidunt voluptatum, deserunt nostrum at officia praesentium, iure natus labore itaque repudiandae? Placeat error quasi minus ut, aliquam sed?
                
                    </p>

                    <h3>Bienvenido a la teoría de la Tierra Plana</h3>
            
                </div>

                <div className="col d-flex align-items-center">
                <Image/>

                </div>
            </div>
            <div className="d-flex justify-content-center mt-5">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/qwjy1DIriI8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    );
}

export default Home;