import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
            <img src="Icon.png" alt="Iconito" height="45" width="45" />
            <a className="navbar-brand ml-3">Terraplaneros</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">

                        <Link to="/" className="nav-link">Home</Link>
                    </li>
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Argumentos
                        </a>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">

                            <Link to="/Arg1" className="dropdown-item">Argumento 1</Link>
                            <Link to="/Arg2" className="dropdown-item">Argumento 2</Link>
                            <Link to="/Arg3" className="dropdown-item">Argumento 3</Link>
                            <Link to="/Arg4" className="dropdown-item">Argumento 4</Link>


                        </div>
                    </li>

                    <li>
                        <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Contacto
</button>
                    </li>
                </ul>

            </div>
        </nav>

    );
}

export default Navbar;