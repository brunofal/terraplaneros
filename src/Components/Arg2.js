import React from 'react';

const Arg2 = () => {
    return (
        <div className="container mt-5 mb-5">
            <div className="row align-middle">

                <div className="col">

                    <h2>La tierra como centro del sistema</h2>
                    <p>
                        En el modelo terraplanista el sol no es el centro de nuestro sistema solar, sino la tierra. Evidentemente, nosotros
                        vemos al sol y la luna moverse por nuestros cielos y asi es como verdaderamente ocurre. ¿Como podríamos nosotros estar girando
                        a la velocidad que proponen los cientificos sin darnos cuenta? ¡Es una locura!
                </p>

                    <h5>
                        Sí, Copérnico le pifió por mucho.
                </h5>


                </div>

                <div className="col d-flex align-items-center">
                    <div className="d-flex justify-content-center align-middle">

                        <img src="https://media1.tenor.com/images/95d0956d8e97a6bc7026ca3ac93971f0/tenor.gif?itemid=7295301" className="rounded img-fluid" alt="Imagencita" />

                    </div>

                </div>
            </div>
       
        </div>
    );
}

export default Arg2;