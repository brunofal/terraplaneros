import React from 'react';
import Arg1 from './Components/Arg1'
import Arg2 from './Components/Arg2'
import Arg3 from './Components/Arg3'
import Arg4 from './Components/Arg4'
import Navbar from './Components/Navbar';
import Footer from './Components/Footer';
import Home from './Components/Home';
import Modal from './Components/Modal';
import './App.css'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Header from './Components/Header';

function App() {
  return (

    <Router>
      <div className="App">
        <Navbar />
        <Modal />
        <Switch>

          <Route path="/Arg1">
          <Header titulo="La falsa curvatura"/>
            <Arg1 />
          </Route>

          <Route path="/Arg2">
          <Header titulo="¿Qué gira alrededor de qué?"/>
            <Arg2 />
          </Route>

          <Route path="/Arg3">
          <Header titulo="¿Che y la gravedad?"/>
            <Arg3 />
          </Route>

          <Route path="/Arg4">
          <Header titulo="Las palabras dicen la verdad"/>
            <Arg4 />
          </Route>

          <Route path="/">
           <Header titulo="Lo que ellos no quieren que sepas"/>
            <Home />
          </Route>

        </Switch>

        <Footer />
      </div>
    </Router>
  );
}

export default App;
